<?php

// LOCA - Localization Engine.

// default language. We probably want to make his dynamic, based on player
$LocaLang = "en";

$LOCA = array ();

// Return the value of the key. Returns the latest value.
// If there is no such a key, return key name.
function loca ($key)
{
	global $LOCA, $LocaLang;
	if ( gettype($LOCA[$LocaLang]) !== "array" )
		return "$key";
	if ( key_exists ( $key, $LOCA[$LocaLang]) )
		return $LOCA[$LocaLang][$key];
	else
		return "$key";
}

// Add a new key value.
function loca_add ($key, $value)
{
	global $LOCA, $LocaLang;
	$LOCA[$LocaLang][$key] = $value;
}

?>