<script type="text/javascript">

function createCookie(name,value,days) {
	var expires;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	else expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function setlang(lang)
{
	createCookie ( "ogamelang", lang, 9999);
	location.reload ();
}

</script>

<div class="products" align="right">

<a href="#" onclick="javascript:setlang('de');"><img src="img/flags/de.gif" alt="Deutschland" title="Deutschland"></a>
<a href="#" onclick="javascript:setlang('en');"><img src="img/flags/gb.gif" alt="English" title="English"></a>
<a href="#" onclick="javascript:setlang('ru');"><img src="img/flags/ru.gif" alt="Russia" title="Russia"></a>

<a href="#"><?php echo loca('CHOOSELANG');?></a>
</div>