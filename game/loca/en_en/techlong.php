<?php

// Expanded descriptions.

$LOCA["en"]["LONG_1"] = "The main supplier of raw materials for the construction of load-bearing structures of buildings and ships. Metal is a very cheap raw material, but it is needed more than anything else. Metal production requires less total energy. The higher the level of the mine, the deeper they go underground. On most planets, the metal is at great depths and these deeper mines can produce more metal as the are improved. However, larger mines require more energy.";

?>