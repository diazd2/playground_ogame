<?php

// Alliance system.

// Abbreviations may not be the same, but the name is.

// The alliance records in the database (ally):
//   Ally_id: id number (INT AUTO_INCREMENT PRIMARY KEY)
//   Tag: 3-8 characters (CHAR (8))
//   Name: 3-30 characters (CHAR (30))
//   Owner_id: ID of founder
//   Homepage: URL home page
//   Imglogo: URL of the logo image
//   Open: 0 - applications not accepted (Alliance closed), 1 - Applications allowed.
//   Insertapp: 1 - automatically substitute the application template, 0 - do not insert a template
//   Exttext: External text - non members (TEXT)
//   Inttext: Inner text - members (TEXT)
//   Apptext: Application text (TEXT)
//   Nextrank: The sequence number of the next rank (INT)
//   Old_tag: Old alliance tag (CHAR (8))
//   Old_name: Old alliance name (CHAR (30))
//   Tag_until: When you can change the alliance's tag (INT UNSIGNED)
//   Name_until: When you can change the alliance's name (INT UNSIGNED)
//   Score1,2,3: Points for buildings, fleet, research (BIGINT UNSIGNED, INT UNSIGNED, INT UNSIGNED)
//   Place1,2,3: Location of buildings, fleet, research (INT)
//   Oldscore1,2,3: Old score for buildings, fleet, research (BIGINT UNSIGNED, INT UNSIGNED, INT UNSIGNED)
//   Oldplace1,2,3: old place of buildings, fleet, research (INT)
//   Scoredate: Time when statistics were last saved (INT UNSIGNED)

// Creates an alliance. Returns ID of the alliance.
function CreateAlly ($owner_id, $tag, $name)
{
    global $db_prefix;
    $tag = mb_substr ($tag, 0, 8, "UTF-8");    // Line-length limit
    $name = mb_substr ($name, 0, 30, "UTF-8");

    // Add alliance.
    $ally = array( null, $tag, $name, $owner_id, "", "", 1, 0, "Welcome to the page of the alliance", "", "", 0, "", "", 0, 0,
                        0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0 );
    $id = AddDBRow ( $ally, "ally" );

    // Add the ranks of "Founder" (0) and "Beginner" (1).
    SetRank ( $id, AddRank ( $id, "Founder" ), 0x1FF );
    SetRank ( $id, AddRank ( $id, "Beginner" ), 0 );

    // Update founder's info.
    $joindate = time ();
    $query = "UPDATE ".$db_prefix."users SET ally_id = $id, joindate = $joindate, allyrank = 0 WHERE player_id = $owner_id";
    dbquery ($query);

    return $id;
}

// Dissolves alliance.
function DismissAlly ($ally_id)
{
    global $db_prefix;

    // Make ally_id and ranks of all the players of the alliance = 0.
    $query = "UPDATE ".$db_prefix."users SET ally_id = 0, joindate = 0, allyrank = 0 WHERE ally_id = $ally_id";
    dbquery ($query);

    // Remove the ranks from the ranks table
    $query = "DELETE FROM ".$db_prefix."allyranks WHERE ally_id = $ally_id";
    dbquery ($query);

    // Remove all pending applications
    $query = "DELETE FROM ".$db_prefix."allyapps WHERE ally_id = $ally_id";
    dbquery ($query);

    // Delete record from alliances table
    $query = "DELETE FROM ".$db_prefix."ally WHERE ally_id = $ally_id";
    dbquery ($query);
}

// List all the players of the alliance.
// Sort: 0 - Coordinates, 1 - Name, 2 - Status, 3 - Points, 4 - Entry, 5 - Online
// Order: 0 - ascending, 1 - descending
function EnumerateAlly ($ally_id, $sort_by=0, $order=0)
{
    global $db_prefix;
    if ($ally_id <= 0) return NULL;

    switch ( $sort_by )
    {
        case 1 : $sort = " ORDER BY oname "; break;
        case 2 : $sort = " ORDER BY allyrank "; break;
        case 3 : $sort = " ORDER BY score1 "; break;
        case 4 : $sort = " ORDER BY joindate "; break;
        case 5 : $sort = " ORDER BY lastclick "; break;
        default : $sort = " ORDER BY player_id "; break;
    }
    if ( $order ) $sort .= " DESC";

    $query = "SELECT u.oname, u.ally_id, u.allyrank, u.score1, u.player_id, u.hplanetid, u.joindate, u.lastclick, r.name, p.g, p.s, p.p " .
			 "	FROM ".$db_prefix."users u " .
			 "	LEFT  JOIN ".$db_prefix."allyranks r ON u.ally_id = r.ally_id AND u.allyrank = r.rank_id " .
			 "  LEFT  JOIN ".$db_prefix."planets p ON u.hplanetid = p.planet_id " .
			 "	WHERE u.ally_id = $ally_id " . $sort;

    $result = dbquery ($query);
    return $result;
}

// Check whether there is an alliance with this tag.
function IsAllyTagExist ($tag)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."ally WHERE tag = '".$tag."'";
    $result = dbquery ($query);
    if (dbrows ($result)) return true;
    else return false;
}

// Load Alliance.
function LoadAlly ($ally_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."ally WHERE ally_id = $ally_id";
    $result = dbquery ($query);
    return dbarray ($result);
}

// Search alliance's tag. Returns the direct result of the SQL-query.
function SearchAllyTag ($tag)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."ally WHERE tag LIKE '%".$tag."%' LIMIT 30";
    $result = dbquery ($query);
    return $result;
}

// Count the number of members in the alliance.
function CountAllyMembers ($ally_id)
{
    global $db_prefix;
    if ( $ally_id <= 0 ) return 0;
    $result = EnumerateAlly ($ally_id);
    return dbrows ($result);
}

// Change the alliance's tag. Can only be done every 7 days.
function AllyChangeTag ($ally_id, $tag)
{
    global $db_prefix;
    $now = time ();
    $ally = LoadAlly ($ally_id);
    if ( $now < $ally['tag_until'] ) return false;    // not yet.
    if ( $ally['tag'] === $tag ) return false;
    $until = $now + 7 * 24 * 60 * 60;
    $query = "UPDATE ".$db_prefix."ally SET old_tag = tag, tag = '".$tag."', tag_until = $until WHERE ally_id = $ally_id";
    dbquery ($query);
    return true;
}

// Change the alliance's name. Can only be done every 7 days.
function AllyChangeName ($ally_id, $name)
{
    global $db_prefix;
    $now = time ();
    $ally = LoadAlly ($ally_id);
    if ( $now < $ally['name_until'] ) return false;    // not yet
    if ( $ally['name'] === $name ) return false;
    $until = $now + 7 * 24 * 60 * 60;
    $query = "UPDATE ".$db_prefix."ally SET old_name = name, name = '".$name."', name_until = $until WHERE ally_id = $ally_id";
    dbquery ($query);
    return true;
}

// Change the alliance's founder
function AllyChangeOwner ($ally_id, $owner_id)
{
    global $db_prefix;
    $query = "UPDATE ".$db_prefix."ally SET owner_id = " . intval($owner_id);
    dbquery ($query);
}

// Calculate the alliance's points, based on the members' points
function RecalcAllyStats ()
{
    global $db_prefix;

    $query = "SELECT * FROM ".$db_prefix."ally ";
    $result = dbquery ( $query );
    $rows = dbrows ( $result );
    while ($rows--)
    {
        $ally = dbarray ( $result );
        $query = "SELECT SUM(score1) AS sum1, SUM(score2) AS sum2, SUM(score3) AS sum3 FROM ".$db_prefix."users WHERE ally_id = " . $ally['ally_id'];
        $res = dbquery ($query);
        if ( dbrows ($res) > 0 ) {
            $score = dbarray ( $res );
            $query = "UPDATE ".$db_prefix."ally SET score1 = '".$score['sum1']."', score2 = '".$score['sum2']."', score3 = '".$score['sum3']."' WHERE ally_id = " . $ally['ally_id'];
            dbquery ( $query );
        }
    }
}

// Calculate rank of all alliances.
function RecalcAllyRanks ()
{
    global $db_prefix;

    // buildings
    dbquery ("SET @pos := 0;");
    $query = "UPDATE ".$db_prefix."ally
              SET place1 = (SELECT @pos := @pos+1)
              ORDER BY score1 DESC";
    dbquery ($query);

    // fleet
    dbquery ("SET @pos := 0;");
    $query = "UPDATE ".$db_prefix."ally
              SET place2 = (SELECT @pos := @pos+1)
              ORDER BY score2 DESC";
    dbquery ($query);

    // research
    dbquery ("SET @pos := 0;");
    $query = "UPDATE ".$db_prefix."ally
              SET place3 = (SELECT @pos := @pos+1)
              ORDER BY score3 DESC";
    dbquery ($query);
}

// ****************************************************************************
// Ranks.

// Allowed characters in rank title: [a-zA-Z0-9_-.]. Max. length - 30 characters
// Names can be the same.
// Max of 25 ranks per alliance.

// 0x001: Dissolve Alliance
// 0x002: Kick out player
// 0x004: View application
// 0x008: View member list
// 0x010: Edit application
// 0x020: Management Alliance
// 0x040: View "online" status in the list of members
// 0x080: Create a common message
// 0x100: 'right hand' (needed for the transmission of founder status)

// Entries for ranks in the database (allyranks).
// Rank_id: Rank ID number (INT)
// Ally_id: Alliance ID
// Name: Name of the rank (CHAR (30))
// Rights: Rights (OR mask)

// Add the rank (with no rights) to the alliance. Returns the rank number.
function AddRank ($ally_id, $name)
{
    global $db_prefix;
    if ($ally_id <= 0) return 0;
    $ally = LoadAlly ($ally_id);
    $rank = array ( $ally['nextrank'], $ally_id, $name, 0 );
    $opt = " (";
    foreach ($rank as $i=>$entry)
    {
        if ($i != 0) $opt .= ", ";
        $opt .= "'".$rank[$i]."'";
    }
    $opt .= ")";
    $query = "INSERT INTO ".$db_prefix."allyranks VALUES".$opt;
    dbquery ($query);
    $query = "UPDATE ".$db_prefix."ally SET nextrank = nextrank + 1 WHERE ally_id = $ally_id";
    dbquery ($query);
    return $ally['nextrank'];
}

// Set rights for the rank.
function SetRank ($ally_id, $rank_id, $rights)
{
    global $db_prefix;
    $query = "UPDATE ".$db_prefix."allyranks SET rights = $rights WHERE ally_id = $ally_id AND rank_id = $rank_id";
    dbquery ($query);
}

// Remove rank.
function RemoveRank ($ally_id, $rank_id)
{
    global $db_prefix;
    $query = "DELETE FROM ".$db_prefix."allyranks WHERE ally_id = $ally_id AND rank_id = $rank_id";
    dbquery ($query);
}

// List all ranks in alliance.
function EnumRanks ($ally_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."allyranks WHERE ally_id = $ally_id";
    return dbquery ($query);
}

// Set user under rank.
function SetUserRank ($player_id, $rank)
{
    global $db_prefix;
    $query = "UPDATE ".$db_prefix."users SET allyrank = $rank WHERE player_id = $player_id";
    dbquery ($query);
}

// Load rank.
function LoadRank ($ally_id, $rank_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."allyranks WHERE ally_id = $ally_id AND rank_id = $rank_id";
    $result = dbquery ($query);
    return dbarray ($result);
}

// Load all users with given rank
function LoadUsersWithRank ($ally_id, $rank_id )
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."users WHERE ally_id = $ally_id AND allyrank = $rank_id ";
    $result = dbquery ($query);
    return $result;
}

// ****************************************************************************
// Applications for membership in the alliance.

// Fields for the applications table in the database (allyapps).
// App_id: application id number (INT AUTO_INCREMENT PRIMARY KEY)
// Ally_id: Alliance ID
// Player_id: user who sent a request
// Text: Text in the application (TEXT)
// Date: Datetime of the application | time () (INT UNSIGNED)

// Add the application to the alliance. Returns the id number of the application.
function AddApplication ($ally_id, $player_id, $text)
{
    $app = array ( null, $ally_id, $player_id, $text, time() );
    $id = AddDBRow ( $app, "allyapps" );
    return $id;
}

// Remove application.
function RemoveApplication ($app_id)
{
    global $db_prefix;
    $query = "DELETE FROM ".$db_prefix."allyapps WHERE app_id = $app_id";
    dbquery ($query);
}

// List all applications.
function EnumApplications ($ally_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."allyapps WHERE ally_id = $ally_id";
    return dbquery ($query);
}

// User has already submitted an application to the Alliance?
// If yes - returns the application ID
// otherwise returns 0.
function GetUserApplication ($player_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."allyapps WHERE player_id = $player_id";
    $result = dbquery ($query);
    if ( dbrows ($result) > 0 )
    {
        $app = dbarray ($result);
        return $app['app_id'];
    }
    else return 0;
}

// Load application.
function LoadApplication ($app_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."allyapps WHERE app_id = $app_id";
    $result = dbquery ($query);
    return dbarray ($result);
}

// ****************************************************************************
// Buddy system (Friends). Not more than 16 friends.

// Entries in the database (buddy)
// Buddy_id: ID of the record in the table (INT AUTO_INCREMENT PRIMARY KEY)
// Request_from: User who sent the request
// Request_to: User who receives the request
// Text: query text (TEXT)
// Accepted: Request confirmed. Users - friends.

// Returns the query ID if the request is sent, or
// 0 if the proposal has already been submitted.
function AddBuddy ($from, $to, $text)
{
    global $db_prefix;
    $text = mb_substr ($text, 0, 5000, "UTF-8");    // line-length limit
    if ($text === "") $text = "пусто";

    // Check if application is not confirmed.
    $query = "SELECT * FROM ".$db_prefix."buddy WHERE ((request_from = $from AND request_to = $to) OR (request_from = $to AND request_to = $from)) AND accepted = 0";
    $result = dbquery ($query);
    if ( dbrows($result) ) return 0;

    // Users are already friends?
    if ( IsBuddy ($from, $to) ) return 0;

    // Add request.
    $buddy = array( null, $from, $to, $text, 0 );
    $id = AddDBRow ( $buddy, "buddy" );
    return $id;
}

// Remove buddy.
function RemoveBuddy ($buddy_id)
{
    global $db_prefix;
    $query = "DELETE FROM ".$db_prefix."buddy WHERE buddy_id = $buddy_id";
    dbquery ($query);
}

// Accept buddy.
function AcceptBuddy ($buddy_id)
{
    global $db_prefix;
    $query = "UPDATE ".$db_prefix."buddy SET accepted = 1 WHERE buddy_id = $buddy_id";
    dbquery ($query);
}

// Load buddy.
function LoadBuddy ($buddy_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."buddy WHERE buddy_id = $buddy_id";
    $result = dbquery ($query);
    return dbarray ($result);
}

// List all requests sent by player.
function EnumOutcomeBuddy ($player_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."buddy WHERE request_from = $player_id AND accepted = 0";
    return dbquery ($query);
}

// List all requests received by player.
function EnumIncomeBuddy ($player_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."buddy WHERE request_to = $player_id AND accepted = 0";
    return dbquery ($query);
}

// List all the player's friends.
function EnumBuddy ($player_id)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."buddy WHERE (request_from = $player_id OR request_to = $player_id) AND accepted = 1";
    return dbquery ($query);
}

// Check if players are buddies.
function IsBuddy ($player1, $player2)
{
    global $db_prefix;
    $query = "SELECT * FROM ".$db_prefix."buddy WHERE ((request_from = $player1 AND request_to = $player2) OR (request_from = $player2 AND request_to = $player1)) AND accepted = 1";
    $result = dbquery ($query);
    if ( dbrows($result)) return true;
    else return false;
}

?>